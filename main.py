import logging
import prawcore.exceptions
import configparser
import os
import traceback
import sys
from libpolarbytebot.transcriber.guildwars2_blog import guildwars2_findall_blog_urls, \
    guildwars2_findall_blog_urls_mentions, guildwars2_blog_parse
from libpolarbytebot.transcriber.guildwars2_forum import guildwars2_findall_forum_urls, \
    guildwars2_findall_forum_urls_mentions, guildwars2_forum_parse
from libpolarbytebot import polarbytebot

__version__ = '0.2.2'
__owner = 'Xyooz'
__source_link = 'https://gitlab.com/networkjanitor/docker-polarbytebot-gw2-submission-transcriber'
__user_agent = f'polarbytebot-gw2-submissions-transcriber/{__version__} by /u/{__owner}'
__signature = f'\n' \
              f'\n' \
              f'---\n' \
              f'^(Beep boop bleep. I\'m a bot. Message me or /u/{__owner} if you have any ' \
              f'questions, suggestions or concerns.) [^Source ^Code]({__source_link})'
__subreddits = 'guildwars2+test+gw2economy'
__microservice_id = 'gw2-subm-tscrb'

__enable_submission_linkposts_transcribing = True
__enable_submission_selftext_transcribing = False
__enable_submission_mentions_transcribing = True


def run():
    path_to_conf = os.path.abspath(os.path.dirname(sys.argv[0]))
    path_to_conf = os.path.join(path_to_conf, 'settings.conf')
    cfg = configparser.ConfigParser()
    cfg.read(path_to_conf)
    pbb = polarbytebot.Polarbytebot(signature=__signature, microservice_id=__microservice_id,
                                    oauth_client_id=cfg.get('oauth2', 'client_id'),
                                    oauth_client_secret=cfg.get('oauth2', 'client_secret'),
                                    oauth_redirect_uri=cfg.get('oauth2', 'redirect_uri'),
                                    oauth_username=cfg.get('oauth2', 'username'), praw_useragent=__user_agent,
                                    oauth_refresh_token=cfg.get('oauth2', 'refresh_token'),
                                    database_system=cfg.get('database', 'system'),
                                    database_username=cfg.get('database', 'username'),
                                    database_password=cfg.get('database', 'password'),
                                    database_host=cfg.get('database', 'host'),
                                    database_dbname=cfg.get('database', 'database'))
    while True:
        safe_poll(pbb)


def safe_poll(pbb):
    try:
        for subm in pbb.reddit.subreddit(__subreddits).stream.submissions():
            if subm.author is None or subm.author.name == pbb.username:
                continue
            if subm.is_self and __enable_submission_selftext_transcribing:
                handle(subm, subm.selftext, pbb)
            elif subm.is_self and __enable_submission_mentions_transcribing:
                handle(subm, subm.selftext, pbb, search_mentions=True)

            if not subm.is_self and __enable_submission_linkposts_transcribing:
                handle(subm, subm.url, pbb)
    except prawcore.exceptions.RequestException as e:
        pass


def handle(submission, content, pbb, search_mentions=False):
    # blog stuff
    if search_mentions:
        blog_urls = guildwars2_findall_blog_urls_mentions(content, pbb.mention_regex)
    else:
        blog_urls = guildwars2_findall_blog_urls(content)

    for url in blog_urls:
        try:
            transcription = guildwars2_blog_parse(url)
        except Exception:
            pbb.add_error(url=url, exception_text=traceback.format_exc())
        else:
            pbb.create_comment(submission.name, transcription)

    # forum stuff
    if search_mentions:
        forum_urls = guildwars2_findall_forum_urls_mentions(content, pbb.mention_regex)
    else:
        forum_urls = guildwars2_findall_forum_urls(content)

    for url in forum_urls:
        try:
            transcription = guildwars2_forum_parse(url)
        except Exception:
            pbb.add_error(url=url, exception_text=traceback.format_exc())
        else:
            pbb.create_comment(submission.name, transcription)


if __name__ == '__main__':
    run()
